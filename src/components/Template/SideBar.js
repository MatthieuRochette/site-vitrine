import React from 'react';
import { Link } from 'react-router-dom';

import ContactIcons from '../Contact/ContactIcons';

const { PUBLIC_URL } = process.env; // set automatically from package.json:homepage

const SideBar = () => (
  <section id="sidebar">
    <section id="intro">
      <Link to="/" className="logo">
        <img src={`${PUBLIC_URL}/images/me.jpg`} alt="" />
      </Link>
      <header>
        <h2>Matthieu Rochette</h2>
        <p><a href="mailto:contact@matthieu-rochette.fr">contact@matthieu-rochette.fr</a></p>
      </header>
    </section>

    <section className="blurb">
      <h2>About this website</h2>
      <p>Please feel free to check out my {' '}
        <Link to="/resume">resume</Link>, {' '}
        <Link to="/projects">projects</Link>, {' '}
        view <Link to="/stats">some statistics</Link>, {' '}
        or <Link to="/contact">contact</Link> me.
      </p>
      <p>(I am not a frontend developer, so I did not design this website. I customised the template available <a href="https://github.com/mldangelo/personal-site">here</a>.)</p>
      <ul className="actions">
        <li>
          <Link to="/resume" className="button">Learn More</Link>
        </li>
      </ul>
    </section>

    <section id="footer">
      <ContactIcons />
      <p className="copyright">&copy; Matthieu Rochette <Link to="/">www.matthieu-rochette.fr</Link>.</p>
    </section>
  </section>
);

export default SideBar;
