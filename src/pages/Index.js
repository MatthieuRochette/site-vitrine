import React from 'react';
import { Link } from 'react-router-dom';

import Main from '../layouts/Main';

const Index = () => (
  <Main
    description={"Matthieu Rochette's personal website. French backend software developer and devops engineer. Studied @ Epitech, worked for Leclanché, Epitech, PwC, Extia and AlphaOmega."}
  >
    <article className="post" id="index">
      <header>
        <div className="title">
          <h2><Link to="/">Welcome!</Link></h2>
          <p>
            Thank you for visiting my website!
          </p>
        </div>
      </header>
      <p>
        Hi, I&apos;m Matthieu. I studied for 5 years
        at <a href="https://www.epitech.eu">Epitech</a> until 2023, including
        several internships, and have been working since as a backend software and devops engineer.
        Check out my experience on my <a href="/resume">resume</a>! I also have done a number
        of <a href="/projects">projects</a> (some of which are still ongoing).
      </p>
      <p>
        My prefered programming language is Python, which I started using back in 2015, so almost
        10 years ago! However, I&apos;m still learning new things with and about it, and I&apos;m
        far from done yet. I also picked up on system administration and devops skills along the
        way, which have served me both professionally and personally more than once!
        For this website, even!
      </p>
    </article>
  </Main>
);

export default Index;
