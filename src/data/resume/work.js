/**
 * @typedef {Object} Position
 * Conforms to https://jsonresume.org/schema/
 *
 * @property {string} name - Name of the company
 * @property {string} position - Position title
 * @property {string} url - Company website
 * @property {string} startDate - Start date of the position in YYYY-MM-DD format
 * @property {string|undefined} endDate - End date of the position in YYYY-MM-DD format.
 * If undefined, the position is still active.
 * @property {string|undefined} summary - html/markdown summary of the position
 * @property {string[]} highlights - plain text highlights of the position (bulleted list)
 */
const work = [
  {
    name: 'AlphaOmega | Luxemburg',
    position: 'Python developer & DevOps',
    url: 'https://www.alphaomega.lu/',
    startDate: '2023-11-15',
    endDate: '2024-10-04',
    summary: `AlphaOmega is a Fintech startup that operates in the asset management sector. They produce financial
    reports and documents for their clients.`,
    highlights: [
      'I worked as a Python developer and DevOps as part of the Performance team.',
      `My main focus was to maintain our systems, monitor the performance and develop
      improvements and optimizations in order to accelerate our production.`,
      `I mainly worked with Openshift, Kubernetes, ArgoCD, Docker, Grafana and Github for the
      devops side of things. On the development front, I used Python with third-party libraries
      such as Pandas, Celery, FastAPI and aiohttp.`,
      `I also gathered experience with various industry-leading storage/databases tools like
      SQL Server, MongoDB, RabbitMQ, Amazon S3 and Redis.`,
    ],
  },
  {
    name: 'Extia | Barcelona',
    position: 'Web Backend and DevOps consultant',
    url: 'https://www.extia-group.com/es-en',
    startDate: '2022-09-01',
    endDate: '2023-10-31',
    summary: `Extia is a consulting company specializing in IT, Engineering and Digital professions.
    I became a consultant in the Barcelona branch.`,
    highlights: [
      'Worked as a DevOps consultant for Amadeus, one of the world leaders for the Travel IT sector. (amadeus.com)',
      'I worked as part of a team dedicated to Amadeus\'s migration to the public cloud on Microsoft Azure.',
      'This mission was an incredible technical challenge as well as an important personal growth opportunity for me.',
      'Worked with Groovy (language), Jenkins, Docker, Kubernetes, Helm, Openshift, IntelliJ IDEA.',
    ],
  },
  {
    name: 'PwC | Luxemburg',
    position: 'Data Collection and Analysis intern',
    url: 'https://www.pwc.lu/',
    startDate: '2021-04-01',
    endDate: '2021-08-31',
    summary: 'PwC is one of the Big Four of the Auditing, Consulting and Financial services world.',
    highlights: [
      'As a Data intern in the Luxemburg firm of the PwC group, I had the opportunity to work closely with innovative natural language provessing AI research.',
      'I also worked with mature data flows, from collection to engineering and even reporting.',
      'Worked with C#/.NET, Python, Microsoft SSIS/SSRS/PowerBI, SQL Server/TSQL',
    ],
  },
  {
    name: 'Ionis STM | Strasbourg, France',
    position: 'Coding Club regional manager (part-time, 40%)',
    url: 'https://codingclub.epitech.eu/atelier-programmation-informatique',
    startDate: '2020-10-01',
    endDate: '2021-03-31',
    summary: 'The Coding Club is a free educational program made by Epitech, which aims to promote and introduce the world of software development to teenagers by the students through playful means.',
    highlights: [
      'As the regional manager of the Coding Club for Strasbourg, France, I had several responsibilities, including organizing events to recruiting a team of teaching assistants and creating the content for those events.',
      'As the Covid19 pandemic was happening, some of the focus was on organizing national-wide online events with other Epitech campuses. Thanks to this, I had the opportunity to work in a 17 member-strong team.',
      'As we organized the online events through Discord, I developed a bot to automate the organization and communication of the events to our participants.',
      'Worked with Python, Java, C, JS and more, to create teaching materials (and, for Python, the Discord bot).',
    ],
  },
  {
    name: 'Leclanché | Willstätt, Germany',
    position: 'IT engineer intern',
    url: 'https://www.leclanche.com/fr/',
    startDate: '2019-07-15',
    endDate: '2019-12-15',
    summary: 'Leclanché is an industrial-grade battery manufacturer, providing electricity storage solutions for clients all over the world.',
    highlights: [
      'I worked in the battery plant in Germany, as an intern in the engineering team.',
      'My assignments were mainly of two natures: data visualisation development for the engineering, R&D and QA teams, as well as systems/network administrator for the plant.',
      'Worked with Python to develop a modular data visualization interface, allowing for various kinds of visualization with high customization and personalized features for the users.',
      'Worked with hardware, both to maintain/upgrade computers and servers on the plant as well as for network administration.',
      'Worked with virtual machines to setup a few internal services on the plant network, such as a team-oriented password manager or Gitlab.',
    ],
  },

];

export default work;
