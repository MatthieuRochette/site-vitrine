const skills = [
  {
    title: 'C#',
    // competency: 5,
    category: ['Languages'],
  },
  {
    title: 'Groovy',
    // competency: 4,
    category: ['Languages'],
  },
  {
    title: 'Bash',
    // competency: 2,
    category: ['Languages'],
  },
  {
    title: 'MongoDB',
    // competency: 3,
    category: ['Databases'],
  },
  {
    title: 'RabbitMQ',
    // competency: 3,
    category: ['Tools'],
  },
  {
    title: 'PostgreSQL / SQLite / SQL Server',
    // competency: 4,
    category: ['Databases'],
  },
  {
    title: 'SQL',
    // competency: 3,
    category: ['Languages'],
  },
  {
    title: 'Redis',
    // competency: 4,
    category: ['Databases'],
  },
  {
    title: 'Flask',
    // competency: 4,
    category: ['Python'],
  },
  {
    title: 'FastAPI',
    // competency: 3,
    category: ['Python'],
  },
  {
    title: 'Git / Perforce / SVN',
    // competency: 4,
    category: ['Tools'],
  },
  {
    title: 'Kubernetes',
    // competency: 3,
    category: ['DevOps'],
  },
  {
    title: 'Docker',
    // competency: 4,
    category: ['DevOps'],
  },
  {
    title: 'Numpy',
    // competency: 3,
    category: ['Python'],
  },
  {
    title: 'Numba',
    // competency: 2,
    category: ['Python'],
  },
  {
    title: 'Jupyter',
    // competency: 3,
    category: ['Python'],
  },
  {
    title: 'HTML + CSS',
    // competency: 3,
    category: ['Languages'],
  },
  {
    title: 'Python',
    // competency: 5,
    category: ['Languages'],
  },
  {
    title: 'C++',
    // competency: 3,
    category: ['Languages'],
  },
  {
    title: 'Pandas',
    // competency: 4,
    category: ['Python'],
  },
  {
    title: 'Matplotlib / Bokeh / Plotly',
    // competency: 3,
    category: ['Python'],
  },
  {
    title: 'Ruff',
    // competency: 4,
    category: ['Python'],
  },
  {
    title: 'Celery',
    // competency: 4,
    category: ['Python'],
  },
  {
    title: 'C',
    // competency: 3,
    category: ['Languages'],
  },
  {
    title: 'Gitlab CI',
    // competency: 5,
    category: ['DevOps'],
  },
  {
    title: 'Github Actions',
    // competency: 5,
    category: ['DevOps'],
  },
  {
    title: 'Jenkins',
    // competency: 4,
    category: ['DevOps'],
  },
  {
    title: 'Openshift',
    // competency: 3,
    category: ['DevOps'],
  },
  {
    title: 'ArgoCD',
    // competency: 3,
    category: ['DevOps'],
  },
  {
    title: 'REST (API)',
    // competency: 5,
    category: ['Concepts'],
  },
  {
    title: 'Linux',
    // competency: 4,
    category: ['AdminSys', 'Tools'],
  },
  {
    title: 'Windows',
    // competency: 4,
    category: ['Tools'],
  },
  {
    title: 'Agile / Kanban / Scrum',
    // competency: 3,
    category: ['Concepts'],
  },
  {
    title: 'Networks (IPv4/6, Subnet, DNS, Proxy, firewall...)',
    // competency: 3,
    category: ['Concepts', 'AdminSys', 'DevOps'],
  },
  {
    title: 'CI/CD',
    // competency: 4,
    category: ['Concepts'],
  },
  {
    title: 'French (mother tongue)',
    // competency: 5,
    category: ['Human Languages'],
  },
  {
    title: 'English (fluent)',
    // competency: 4,
    category: ['Human Languages'],
  },
  {
    title: 'German (good comprehension, intermediate expression)',
    // competency: 3,
    category: ['Human Languages'],
  },
];

console.log(skills);

// this is a list of colors that I like. The length should be === to the
// number of categories. Re-arrange this list until you find a pattern you like.
const colors = [
  '#6968b3',
  '#feb191',
  '#51afd4',
  '#e47272',
  '#98e662',
  '#a3423f',
  '#f75858',
  '#747fff',
  '#64cb7b',
];

const categories = [
  ...new Set(skills.flatMap(({ category }) => category)),
].map((category, index) => ({
  name: category,
  color: colors[index],
}));

console.log(categories);

export { categories, skills };
