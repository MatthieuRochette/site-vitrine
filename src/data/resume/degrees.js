const degrees = [
  {
    school: 'Epitech',
    degree: 'Expert in Information Technologies (Master\'s degree)',
    link: 'https://www.francecompetences.fr/recherche/rncp/17286/',
    year: 2023,
  },
  {
    school: 'Highschool Michel de Montaigne (Mulhouse, France)',
    degree: 'High School diploma (w/ IT Specialty & additional English & German courses, obtained with High Honors)',
    link: 'https://en.wikipedia.org/wiki/Baccalaur%C3%A9at',
    year: 2018,
  },
];

export default degrees;
