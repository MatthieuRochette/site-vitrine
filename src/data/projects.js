const data = [
  {
    status: 'finished',
    title: 'Flexibip',
    subtitle: 'Epitech Innovative Project - 1st place winner',
    image: '/images/projects/flexibip.jpg',
    date: '2023-08-01',
    link: 'https://www.flexibip.com',
    desc:
      `Flexibip is a team management solution for volunteer firefighters. 
      Our goal is to improve the management of the availability of each 
      team member in real time, to avoid any loss of time during an intervention.`,
  },
  {
    status: 'finished',
    title: 'Fashion MNIST',
    subtitle: 'Deep learning AI project',
    image: '/images/projects/fashion_mnist.png',
    date: '2021-12-01',
    link: 'https://fashion-mnist.matthieu-rochette.fr',
    desc:
      `This project is my first attempt at creating a Deep Learning AI model.
      The goal is for the AI to look at a picture or a piece of clothing, and determine what type of clothing it is.`,
  },
  {
    status: 'finished',
    title: 'Instagram automatic posting',
    subtitle: 'Python script that takes control of the PC to create posts automatically on Instagram.',
    image: '/images/projects/instagram-automation.png',
    date: '2023-04-01',
    link: 'https://gitlab.com/MatthieuRochette/insta-post-automatized/-/blob/master/main.py',
    desc:
      `If you go look at my Instagram profile, you may see that I post a lot of photographs (which is a hobby of mine!).
      Most if not all of these posts are automated through the Meta Business Suite, a website provided by Meta/Facebook
      to manage Instagram and Facebook pages, thanks to this script.`,
  },
  {
    status: 'finished',
    title: 'JSON translation scripts',
    subtitle: 'I was tired of translating hundreds of sentences myself.',
    image: '/images/projects/translation.png',
    date: '2022-12-01',
    link: 'https://gitlab.com/MatthieuRochette/json-auto-update-translate',
    desc:
      `I was translating hundreds of sentences for Flexibip's website, when it struck me: it could be automatized easily.
      So, 4 hours later, here we were: using the Deepl API to translate in my stead, while I could enjoy a hot beverage.`,
  },
  {
    status: 'paused',
    title: 'Music Migrator',
    subtitle: 'An ambitious project to help migrate automatically from one music streaming platform to another.',
    image: '/images/projects/music-migrator.jpg',
    date: '2022-08-01',
    link: 'https://gitlab.com/music-migrator/music-migrator-core',
    desc:
      `The goal of this project is to create a free, simple-to-use CLI or GUI (and even later, potentially web-available)
      providing automation for people looking to migrate from one music streaming platform to another.`,
  },
  {
    status: 'ongoing',
    title: 'See my Github for more',
    subtitle: 'School and personal projects.',
    image: '/images/projects/github.png',
    date: '2018-01-01',
    link: 'https://github.com/MatthieuRochette',
    desc:
      'You can see all my other projects (that I\'m willing to show) here.',
  },
  {
    status: 'ongoing',
    title: 'See my Gitlab for more',
    subtitle: 'School and personal projects.',
    image: '/images/projects/gitlab.png',
    date: '2018-01-01',
    link: 'https://gitlab.com/MatthieuRochette',
    desc:
      'You can see all my other projects (that I\'m willing to show) here.',
  },
  {
    status: 'finished',
    title: '3D video game',
    subtitle: 'First of a series of 3D video games made for school (4th year)',
    image: '/images/projects/capstone.png',
    date: '2022-02-01',
    link: 'https://github.com/MatthieuRochette/Capstone-3D-Platformer',
    desc:
      'Download the ZIP file and give it a try ! (Unzip the file and double click on the .exe file, it is not a virus, promise !)',
  },
  {
    status: 'finished',
    title: 'Gomoku',
    subtitle: 'An AI that plays Gomoku.',
    image: '/images/projects/gomoku.jpg',
    date: '2021-05-01',
    link: 'https://github.com/MatthieuRochette/gomoku',
    desc:
      'Plays better than myself! (Admittedly, I\'m not good at the game)',
  },
  {
    status: 'finished',
    title: 'IoT project',
    subtitle: 'An IoT Dashboard simulation',
    image: '/images/projects/iot.png',
    date: '2021-11-01',
    link: 'https://github.com/MatthieuRochette/Iot_Project_Epitech',
    desc:
      'Both the Dashboard, as a Thingsboard web page, and a desktop app sending simulation data.',
  },
  {
    status: 'finished',
    title: 'Recipe to the perfect video game',
    subtitle: 'Epitech 4th year, Data Science project.',
    image: '/images/projects/vg.jpg',
    date: '2021-10-01',
    link: 'https://deepnote.com/@Data-Science-Epitech-M-TRC-839/Recipe-to-the-perfect-video-game-2a9ec7ae-4a3f-4454-8a31-765f030573ac',
    desc:
      'An article on how to make the perfect video game that most people will love. Quite a fun read, if I say so myself.',
  },
  {
    status: 'finished',
    title: 'Natural Language Processing (NLP) project',
    subtitle: 'Intro to NLP Machine Learning at Epitech.',
    image: '/images/projects/NLP.png',
    date: '2021-11-01',
    link: 'https://github.com/MatthieuRochette/Machine-Learning-For-NLP/blob/main/main.ipynb',
    desc:
      'A very technical project, that I put in this list to make myself look more skilled than I am.',
  },
];

export default data;
